


var cloudVector = $('.cloud');
var flyingVector = $('.flying');

var scrollBaseSpeed = 10;


$(window).load(function() {
	
	$(window).scrollTop(1); // so scroll up/down event is available from beginning
	
	randomizeCloudSize();
	randomizeFlyingObjectPositions();
});

window.onscroll = function(e) {
	moveFlyingObjects();
	};


function randomizeFlyingObjectPositions() {
	
	var cloudHorizontal = 0;
	var cloudVertical = 0;
	var cloudSpeed = 0;
	
	flyingVector.each(function () {
		var flyingObject = $(this);
		
		cloudHorizontal =  Math.floor(30+Math.random()*180);
		cloudVertical =  Math.floor(30+(window.innerHeight-600)*Math.random());
		cloudSpeed = Math.random()*3;
		
		flyingObject.css('left',cloudHorizontal+"px");
		flyingObject.css('top',cloudVertical+"px");
		flyingObject.attr('data-speed', cloudSpeed);
		
	});
}

function randomizeCloudSize() {
	var cloudSize = 0;
	
	cloudVector.each(function() {
		var cloud = $(this);
		
		cloudSize = Math.floor(140+Math.random()*180);
		
		cloud.css('font-size', cloudSize+"px");
	});
}


function moveFlyingObjects(event) {
	
	if ($(window).scrollTop() == 1) return false;
	
	// determine scroll direction
	var scrollPos = $(window).scrollTop();
	var scrollDirection = (scrollPos==0?'up':'down');
	
	// undo scroll
	$(window).scrollTop(1); 
	
	// move clouds
	flyingVector.each(function() {
		var flying = $(this);
		var flyingLeftPos = parseInt(flying.css('left'));
		var offset = Math.floor(10*flying.attr('data-speed'));
		
		if (scrollDirection == 'down') {
			flying.css('left',flyingLeftPos-offset+'px');
		} else { 
			flying.css('left',flyingLeftPos+2*offset+'px');
		}
	});
	console.log('scrolled');
	return false;
}
