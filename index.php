<!DOCTYPE html>
<html>
  <head>
    <title>Clouds - a jQuery & CSS3 animation experiment by Zita Orban</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <!-- Bootstrap - ->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
    <!-- Custom theme -->
    <link href="assets/css/style.css" rel="stylesheet">

  </head>
  <body>
  	
  	
  	<div class='layer clouds' id='clouds'>
		
		 
		<div class='flying'>
			<div class="banner banner-medium banner-orange"><span>hello there</span></div>
			<p class='statement'>
				This is a web animation experiment that uses <strong>no images</strong>.
			</p>
		</div>
		
		<div class='flying'>
			<div class="banner banner-small banner-blue"><span>technologies</span></div>
			<p> 
				It was built entirely with standards-compliant web technologies such as: 
			</p>
			<ul>
				<li>Google webfonts</li>
				<li>HTML5</li>
				<li>CSS3 + LESS</li>
				<li>JavaScript, jQuery</li>
			</ul>
		</div>
		
		
		<div class="flying banner banner-small banner-fuchsia"><span>now scroll your mouse to begin &raquo;</span></div>
		<span class="flying cloud glyphicon glyphicon-cloud"></span> 
		<span class="flying cloud glyphicon glyphicon-cloud"></span> 
		<span class="flying banner banner-large banner-white"><span>lorem ipsum</span></span>  
		<span class="flying cloud glyphicon glyphicon-cloud"></span> 
		<span class="flying cloud glyphicon glyphicon-cloud"></span> 
		<span class="flying banner banner-small banner-orange"><span>sit amet</span></span> 
		<span class="flying cloud glyphicon glyphicon-cloud"></span> 
		<span class="flying banner banner-medium banner-fuchsia"><span>dolor</span></span> 
		<span class="flying cloud glyphicon glyphicon-cloud"></span> 
		<span class="flying cloud glyphicon glyphicon-cloud"></span> 
		<span class="flying cloud glyphicon glyphicon-cloud"></span> 
		<span class="flying cloud glyphicon glyphicon-cloud"></span> 
		<span class="flying banner banner-medium banner-blue"><span>lorem</span></span> 
		<span class="flying cloud glyphicon glyphicon-cloud"></span> 
		<span class="flying cloud glyphicon glyphicon-cloud"></span> 
		<span class="flying banner banner-large banner-green"><span>ipsum</span></span> 
		<span class="flying cloud glyphicon glyphicon-cloud"></span> 
	</div>



    <script src="assets/js/jquery-2.0.3.min.js"></script>	
    <script src="assets/js/main.js"></script>

  </body>
</html>